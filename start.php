<?php

class Moneda {
    const AGUILA = 'AGUILA';
    const SOL = 'SOL';

    public static $conteo;

    public static function lanzar(){
        $numero = random_int(0, 255);
        if($numero >= 0 && $numero <= 125){
            if(!isset(self::$conteo[self::AGUILA])){
                self::$conteo[self::AGUILA] = 1;
            } else {
                self::$conteo[self::AGUILA] += 1;
            }
            return Moneda::AGUILA; 
        } else {
            if(!isset(self::$conteo[self::SOL])){
                self::$conteo[self::SOL] = 0;
            } else {
                self::$conteo[self::SOL] += 1;
            }
            return Moneda::SOL;
        }
    }
}

class Jugador {
    
    protected $id;
    protected $supone;
    protected $juegosGanados;
    protected $juegosPerdidos;
    protected $gano;

    public function __construct($id){
        $this->id = $id;
        $this->juegosGanados = 0;
        $this->juegosPerdidos = 0;
    }
    
    public function getScore(){
        $score = sprintf("Jugador #%s Ganados = %s Perdidos = %s\n", 
        $this->id, 
        $this->juegosGanados, 
        $this->juegosPerdidos);
        return $score;
    }

    public function getId(){
        return $this->id;
    }

    public function getGanados(){
        return $this->juegosGanados;
    }

    public function getPerdidos(){
        return $this->juegosPerdidos;
    }

    public function adivinar(){
        $this->supone = Moneda::lanzar();
        return $this->supone;
    }

    public function cualGana($cara) {
        if( $cara == $this->supone){
            $this->gano = true;
            $this->juegosGanados += 1; 
        } else {
            $this->gano = false;
            $this->juegosPerdidos += 1;
        }
        return $this->gano;
    }

    public function haGanado(){
        if($this->gano){
            return "SI";
        } else {
            return "NO";
        }
    }
}

class Juego {

    protected $cuantasRondas = 0; 
    protected $rondas;
    protected $jugadores;
    
    public function __construct(int $cuantas=0){
        if ($cuantas > 0)
            $this->cuantasRondas = $cuantas;
        else
            $this->cuantasRondas = 1;
    }

    public function addJugador($jugador) {
        if(!isset($this->jugadores)){
            $this->jugadores = [];
        }
        array_push($this->jugadores, $jugador);    
    }

    public function getJugadores(){
        return $this->jugadores;
    }

    public function jugar() {
        echo "Rondas: " . $this->cuantasRondas . "\n";

        for ($i=1; $i <= $this->cuantasRondas; ++$i) {
            foreach($this->jugadores as $j){
                $j->adivinar();
            }

            $resultado = Moneda::lanzar();

            foreach($this->jugadores as $j){
                $j->cualGana($resultado);
            }

            echo "Jugar Ronda #$i... Salio $resultado \n";
            foreach($this->jugadores as $j){
                printf("Jugador #%s gano? %s\n", $j->getId(), $j->haGanado());
            }
        }
    }
}

// Main
$juego = new Juego(3);

$juego->addJugador(new Jugador(1));
$juego->addJugador(new Jugador(2));
$juego->addJugador(new Jugador(3));
$juego->addJugador(new Jugador(4));

$juego->jugar();

echo "Conteos\n";
echo "AGUILA: " . Moneda::$conteo[Moneda::AGUILA];
echo "\n";
echo "SOL: " . Moneda::$conteo[Moneda::SOL];
echo "\n";
echo "Diferencia:" . (Moneda::$conteo[Moneda::AGUILA] - Moneda::$conteo[Moneda::SOL]);
echo "\n";

foreach($juego->getJugadores() as $j){
    echo $j->getScore();
} 
